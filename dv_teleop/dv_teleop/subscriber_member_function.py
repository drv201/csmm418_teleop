# Copyright 2016 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy
from rclpy.node import Node

from std_msgs.msg import String
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist

from rclpy.qos import qos_profile_system_default


class ScanSubscriber(Node):

    def __init__(self):
        super().__init__('scan_subscriber')
        self.subscription = self.create_subscription(
            LaserScan,
            '/scan',
            self.listener_callback,
            10)

# Add a subscriber here to /odom to find out where the robot has been
# To really spice things up, also use IMU
# Alternatively, use some SLAM code to give your absolute position

        self.subscription  # prevent unused variable warning

        self.publisher_ = self.create_publisher(Twist, '/cmd_vel',
                                    qos_profile_system_default)
        

    def listener_callback(self, msg):
        self.get_logger().info('I heard: "%s" and "%s"' % (msg.ranges[100], len(msg.ranges)))
        
        twist = Twist()
        twist.linear.x = 0.5; twist.linear.y = 0.0; twist.linear.z = 0.0
        twist.angular.x = 0.0; twist.angular.y = 0.0; twist.angular.z = -0.5
        self.publisher_.publish(twist)





def main(args=None):
    rclpy.init(args=args)

    scan_subscriber = ScanSubscriber()

    rclpy.spin(scan_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    scan_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
